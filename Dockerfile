FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG KAVITA_VERSION="v0.7.14"

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L https://github.com/Kareadita/Kavita/releases/download/${KAVITA_VERSION}/kavita-linux-x64.tar.gz | tar --no-same-owner -xvzf - -C /app/code/
RUN chmod +x /app/code/Kavita/Kavita
COPY start.sh /app/code/
RUN chmod +x /app/code/start.sh


ENV DOTNET_RUNNING_IN_CONTAINER=true
USER cloudron
WORKDIR /app/data
CMD ["/app/code/start.sh"]
